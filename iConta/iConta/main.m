//
//  main.m
//  iConta
//
//  Created by Studente on 26/10/16.
//  Copyright © 2016 Bragagnolo_Carraro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
