//
//  AppDelegate.h
//  iConta
//
//  Created by Studente on 26/10/16.
//  Copyright © 2016 Bragagnolo_Carraro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

