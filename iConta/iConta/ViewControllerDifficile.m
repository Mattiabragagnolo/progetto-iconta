//
//  ViewControllerDifficile.m
//  iConta
//
//  Created by Studente on 23/11/16.
//  Copyright © 2016 Bragagnolo_Carraro. All rights reserved.
//

#import "ViewControllerDifficile.h"

@interface ViewControllerDifficile ()

@end

@implementation ViewControllerDifficile

- (void)viewDidLoad {
    _btn1.enabled = false;
    _btn2.enabled = false;
    _btn3.enabled = false;
    _btn4.enabled = false;
    _btn5.enabled = false;
    _btn6.enabled = false;
    _btn8.enabled = false;
    _btn9.enabled = false;
    _btn10.enabled = false;
    _btn12.enabled = false;
    _btn13.enabled = false;
    _btn16.enabled = false;
    _btn17.enabled = false;
    _btn18.enabled = false;
    _btn19.enabled = false;
    _btn20.enabled = false;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


int cont = 0;
int temp =0;


-(int)click:(NSInteger)n{
    temp=temp+n;
    NSString *string = [NSString stringWithFormat:@"%d", temp];
    [_txtRisultato setText: string];
    return temp;
}

- (IBAction)btn:(id)sender {
    UIButton *button = (UIButton*)sender;

    
    if (cont > 3)
    {
        UIAlertView * alertMosseSuperate = [[UIAlertView alloc] initWithTitle:@""
            message:@"Non puoi fare più di tre mosse!!!!"
            delegate:self
            cancelButtonTitle:@"Rigioca"
            otherButtonTitles: nil];
        [alertMosseSuperate show];
        temp = 0;
        _txtRisultato.text = @"";
        _txtNumero.text = @"";
        _txtNumero.enabled = true;
        cont=0;
        
    
    }
    else{
        NSString *string = button.titleLabel.text;
    int temp = [string intValue];
    [self click:temp];
    }
    cont++;
    
    button.enabled= false;

}

- (IBAction)txtNumero:(id)sender {
    int num = [_txtNumero.text intValue];
    
    
    if(num > 26)
    {
        UIAlertView *alertNtroppoGrande = [[UIAlertView alloc] initWithTitle:@"Attenzione"
            message:@"Non puoi mettere un numero più grande di 27"
            delegate:self
            cancelButtonTitle:@"Return"
            otherButtonTitles: nil];
        [alertNtroppoGrande show];
        
        
        
    }

}

- (IBAction)btnConferma:(id)sender {
    [self textFieldShouldReturn:_txtNumero];
    _txtNumero.enabled = NO;

    _btn1.enabled = true;
    _btn2.enabled = true;
    _btn3.enabled = true;
    _btn4.enabled = true;
    _btn5.enabled = true;
    _btn6.enabled = true;
    _btn8.enabled = true;
    _btn9.enabled = true;
    _btn10.enabled = true;
    _btn12.enabled = true;
    _btn13.enabled = true;
    _btn16.enabled = true;
    _btn17.enabled = true;
    _btn18.enabled = true;
    _btn19.enabled = true;
    _btn20.enabled = true;
    
}

- (IBAction)btnVerifica:(id)sender {
    if (_txtRisultato.text == _txtNumero.text)
    {UIAlertView *alertHaifattoGiusto = [[UIAlertView alloc] initWithTitle:@""
        message:@"Bravo hai fatto giusto!!!!"
        delegate:self
        cancelButtonTitle:@"Rigioca"
        otherButtonTitles: nil];
        [alertHaifattoGiusto show];
        
        temp = 0;
        _txtRisultato.text = @"";
        _txtNumero.text = @"";
        _txtNumero.enabled = YES;    }
    else{
        UIAlertView *alertHaiSbagliato = [[UIAlertView alloc] initWithTitle:@""
            message:@"No hai sbagliato riprova"
            delegate:self
            cancelButtonTitle:@"Rigioca"
            otherButtonTitles: nil];
        [alertHaiSbagliato show];
        temp = 0;
        _txtRisultato.text = @"";
        _txtNumero.text = @"";
        _txtNumero.enabled = YES;      }
    
    
    _btn1.enabled = false;
    _btn2.enabled = false;
    _btn3.enabled = false;
    _btn4.enabled = false;
    _btn5.enabled = false;
    _btn6.enabled = false;
    _btn8.enabled = false;
    _btn9.enabled = false;
    _btn10.enabled = false;
    _btn12.enabled = false;
    _btn13.enabled = false;
    _btn16.enabled = false;
    _btn17.enabled = false;
    _btn18.enabled = false;
    _btn19.enabled = false;
    _btn20.enabled = false;
    
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


@end
