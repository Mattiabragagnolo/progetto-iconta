//
//  ViewController.m
//  iConta
//
//  Created by Studente on 26/10/16.
//  Copyright © 2016 Bragagnolo_Carraro. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    _txtRisultato.enabled = false;
    _btn1.enabled = false;
    _btn2.enabled = false;
    _btn3.enabled = false;
    _btn4.enabled = false;
    _btn5.enabled = false;
    _btn6.enabled = false;
    _btn8.enabled = false;
    _btn9.enabled = false;
    _btn10.enabled = false;
    _btn12.enabled = false;
    _btn13.enabled = false;
    _btn16.enabled = false;
    _btn23.enabled = false;
    _btn27.enabled = false;
    _btn28.enabled = false;
    _btn30.enabled = false;
    _btn31.enabled = false;
    _btn33.enabled = false;
    _btn35.enabled = false;
    _btn36.enabled = false;

    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


int a = 0;



-(int)click:(NSInteger)n{
    a=a+n;
    NSString *s = [NSString stringWithFormat:@"%d", a];
    [_txtRisultato setText: s];
    return a;
}


- (IBAction)btn:(id)sender {
    
    UIButton *b = (UIButton*)sender;
    NSString *string = b.titleLabel.text;
    int a = [string intValue];
    [self click:a];
    b.enabled = false;
    
    
}

- (IBAction)btnVerifica:(id)sender {
    if (_txtRisultato.text == _txtNumero.text)
    {UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
        message:@"Bravo hai fatto giusto!!!!"
        delegate:self
        cancelButtonTitle:@"Rigioca"
        otherButtonTitles: nil];
        [alert show];
        
        a = 0;
        _txtRisultato.text = @"";
        _txtNumero.text = @"";
          _txtNumero.enabled = YES;
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
            message:@"No hai sbagliato riprova"
            delegate:self
            cancelButtonTitle:@"Rigioca"
            otherButtonTitles: nil];
        [alert show];
        a = 0;
        _txtRisultato.text = @"";
        _txtNumero.text = @"";
          _txtNumero.enabled = YES;        }
    
    
    _btn1.enabled = false;
    _btn2.enabled = false;
    _btn3.enabled = false;
    _btn4.enabled = false;
    _btn5.enabled = false;
    _btn6.enabled = false;
    _btn8.enabled = false;
    _btn9.enabled = false;
    _btn10.enabled = false;
    _btn12.enabled = false;
    _btn13.enabled = false;
    _btn16.enabled = false;
    _btn23.enabled = false;
    _btn27.enabled = false;
    _btn28.enabled = false;
    _btn30.enabled = false;
    _btn31.enabled = false;
    _btn33.enabled = false;
    _btn35.enabled = false;
    _btn36.enabled = false;
    
    
    
}



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        if (buttonIndex == 0)
        {
            a = 0;
            _txtRisultato.text = @"";
            _txtNumero.text = @"";
            
        }
        else
        {
            NSLog(@"user pressed Button Indexed 1");
            
        }
    }
}

- (IBAction)btnConferma:(id)sender {
    
    [self textFieldShouldReturn:_txtNumero];
    _txtNumero.enabled = NO;
    _btn1.enabled = true;
    _btn2.enabled = true;
    _btn3.enabled = true;
    _btn4.enabled = true;
    _btn5.enabled = true;
    _btn6.enabled = true;
    _btn8.enabled = true;
    _btn9.enabled = true;
    _btn10.enabled = true;
    _btn12.enabled = true;
    _btn13.enabled = true;
    _btn16.enabled = true;
    _btn23.enabled = true;
    _btn27.enabled = true;
    _btn28.enabled = true;
    _btn30.enabled = true;
    _btn31.enabled = true;
    _btn33.enabled = true;
    _btn35.enabled = true;
    _btn36.enabled = true;

    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}





- (IBAction)prova:(id)sender {
}
@end
